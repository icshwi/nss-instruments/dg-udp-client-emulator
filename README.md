./git_hash_rd_ex.sh performs three seperate register reads to get the three git hash register values

You may need to modify the files to work with a specific IP address. 

**host_read.py**

`sudo python3 host_read.py <address>`

where address is written in hex e.g:

`0xc0001018`

Returns the whole message as in:

`b'e55c0002c0001018bfd3142a'`


* 0xE55C002   -    is the header and message type
* 0xC0001018  -    is the address
*             -    the last part is the value


**host_write.py**

`sudo python3 host_read.py <address> <data>`

address and data must be in hex 


~~~
[iocuser@ics-iocmach-01 dgro_sctl_python]$ sudo python3 host_write.py 0xC0001000 0xDEAD
Message is: e55c0003c0001000dead
b'e55c0004c0001000dead10ad'
~~~


**Testing**

The directory /test contains a library dg_protocol.py which implements read and write of blocks. The file test_all.py contain tests
which test the functionality of the firmware against the spec. All tests read from the output file of the det_param_gen script so they know 
what access (ro/rw) the register are and what the addresses are.

* test_all_ro() - tries to read from all read only parameters
* test_all_rw() - tries to write to and read from all read write registers, checking the output
* test_unwriteable() - tries to write to read only registers

All tests check for the header and message type response

To run the tests

`pytest -v test_all.py`


