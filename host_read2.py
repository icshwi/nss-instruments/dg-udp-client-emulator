import socket
import sys
import binascii

if len(sys.argv) != 2:
    print("Error - please specify address.")
    sys.exit()


DEST_IP_ADDR = '192.168.10.2'
DEST_UDP_PORT = 65535
BUFFER_SIZE = 1024

READ_HEAD = hex(int("0xE55C0001", 0))[2:]

ADDR = int(sys.argv[1], 0)

MESSAGE = hex(ADDR)[2:] + hex(ADDR+4)[2:] #two consecutive addresses

print('Message is: ' + str(READ_HEAD) + str(MESSAGE))

MESSAGE_BYTES = bytes.fromhex(READ_HEAD) + bytes.fromhex(MESSAGE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = (DEST_IP_ADDR, DEST_UDP_PORT)
sent = sock.sendto(MESSAGE_BYTES, host)

rec_data, rec_host = sock.recvfrom(BUFFER_SIZE)

print(binascii.hexlify(rec_data))

sock.close()
