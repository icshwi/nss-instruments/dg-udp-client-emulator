import socket
import sys
import binascii

if len(sys.argv) != 3:
    print("Error - please specify address and data.")
    sys.exit()


DEST_IP_ADDR = '192.168.10.2'
DEST_UDP_PORT = 65535
BUFFER_SIZE = 1024

WRITE_HEAD = hex(int("0xE55C0003", 0))[2:]

ADDR = int(sys.argv[1], 0)
DATA = int(sys.argv[2], 0)

PAYLOAD = hex(ADDR)[2:] + hex(DATA)[2:].zfill(8)

print('Message is: ' + WRITE_HEAD + PAYLOAD)

PAYLOAD_BYTES = bytes.fromhex(WRITE_HEAD) + bytes.fromhex(PAYLOAD)


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = (DEST_IP_ADDR, DEST_UDP_PORT)
sent = sock.sendto(PAYLOAD_BYTES, host)

rec_data, rec_host = sock.recvfrom(BUFFER_SIZE)

print(binascii.hexlify(rec_data))

sock.close()
