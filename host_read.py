import socket
import sys
import binascii

if len(sys.argv) != 2:
    print("Error - please specify address.")
    sys.exit()


DEST_IP_ADDR = '192.168.10.2'
DEST_UDP_PORT = 65535
BUFFER_SIZE = 1024

READ_HEAD = hex(int("0xE55C0001", 0))[2:]

ADDR = int(sys.argv[1], 0)

MESSAGE = hex(ADDR)[2:]

print('Message is: ' + str(READ_HEAD) + str(MESSAGE))

MESSAGE_BYTES = bytes.fromhex(READ_HEAD) + bytes.fromhex(MESSAGE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = (DEST_IP_ADDR, DEST_UDP_PORT)
sent = sock.sendto(MESSAGE_BYTES, host)

rec_data, rec_host = sock.recvfrom(BUFFER_SIZE)

hex_response = (str(binascii.hexlify(rec_data))[2:-1]).upper()

recHeader = hex_response[0:4]
assert recHeader == "E55C"
print("0x" + recHeader)


recMessage = hex_response[4:8]
assert recMessage == "0002"
print("0x" + recMessage)

recAddress = hex_response[8:16]
assert recAddress == MESSAGE.upper()
print("0x" + recAddress)

recValue = hex_response[16:]
print("0x" + recValue)


sock.close()
