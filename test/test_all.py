import socket
import sys
import binascii
import os
from pathlib import Path 
import dg_protocol as dgro
import select
import pytest


    
PARAM_DIR = "../../output/EPICS"

IP="192.168.10.3"
DEST_UDP_PORT = 65535
buf_len = 1024


host = (IP, DEST_UDP_PORT)
    

def test_all_ro():
    #Tries to read from all ro registers in blocks if they are defined in blocks as parameters
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0.2)
    
    for filename in os.listdir(PARAM_DIR):
        
        if checkIsParam(filename):
            
            filename = Path(str(PARAM_DIR) + "/" + str(filename))  
            # get file pointers for input json and output VHDL files
            fin_json, param_list = dgro.json_parse(filename)
            
            for param_entry in param_list:
                       
                if "access" in param_entry:
                      
                    if param_entry["access"] == "RO":
                                                       
                        #Read it (includes some tests)
                        val = dgro.read_udp(param_entry["offset"],sock,host,buf_len)
                
    sock.close()            
                
    
    
def test_all_rw():
    #Tries to write to all rw registers in blocks if they are defined in blocks as parameters
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0.2)
    
   
        
    for filename in os.listdir(PARAM_DIR):
        
        if checkIsParam(filename):
            
            filename = Path(str(PARAM_DIR) + "/" + str(filename))  
             
            # get file pointers for input json and output VHDL files
            fin_json, param_list = dgro.json_parse(filename)
       
            for param_entry in param_list:
                
                
                if "access" in param_entry:
                       
                    if param_entry["access"] == "RW":
                        
                        #Define the data
                        data_list =     param_entry["offset"] # just use the address offsets as the dummy data
                        
                        #Write it (includes some tests)
                        dgro.write_udp(param_entry["offset"],param_entry["offset"],sock,host,buf_len)
                        
                        #Read it (includes some tests)
                        val = dgro.read_udp(param_entry["offset"],sock,host,buf_len)
                        
                        #Test it
                        print("Testing RW: " + param_entry["label"])
                        for index in range(len(param_entry["offset"])) :
                            assert val[index] == ((data_list[index])[2:]).upper()
                
    sock.close()    
    
#def test_unwriteable():
    #Tries to write to all ro registers in blocks if they are defined in blocks as parameters
    #Tests for not writeable response
    
 #   sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  #  sock.settimeout(0.2)
   
    # get file pointers for input json and output VHDL files
   # fin_json, param_list = dgro.json_parse(REGISTER_MAP_FILE)
      
   # for param_entry in param_list: 
        
    #    if "access" in param_entry:
            
     #       if param_entry["access"] == "RO":
                                 
                #Try and write to it and test that there is an error message received
      #          assert dgro.write_udp(param_entry["offset"],param_entry["offset"],sock,host,buf_len) =="1006"

                
   # sock.close()    
    
#check if a file in the parameter directory
def checkIsParam(filename):

    if "param_map" and ".json" in str(filename):
        return True 
    else: 
        return False


        
    



