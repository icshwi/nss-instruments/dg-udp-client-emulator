import socket
import sys
import binascii
import json



#read a list of addresses. Assumes that the addresses are hex strings: '0xFFFFFFFF'
def read_udp(address_list,sock, host,buf):

    #For each address in the array add it to the request
    header = hex(int("0xE55C0001", 0))[2:]
    
    message_bytes = bytes.fromhex(header)
    
    for index in range(len(address_list)) :
               
        message_bytes += bytes.fromhex(hex(int(address_list[index], 0))[2:])
        
    
    #Make the request
    sent = sock.sendto(message_bytes, host)

    #Parse the response, adding it to a response list
    rec_data, rec_host = sock.recvfrom(buf)
    
    hex_response = (str(binascii.hexlify(rec_data))[2:-1]).upper()

    recHeader = hex_response[0:4]
    print("Testing Header for: " + str(address_list))
    assert recHeader == "E55C"
    


    recMessage = hex_response[4:8]
    print("Testing Message for : " + str(address_list))
    assert recMessage == "0002"
    

    index = 8
    vals = []
    for address in address_list:
    
        recAddress = hex_response[index:index+8]
      
        #Test that the address we are sent is what we asked for
        print("Testing Address for : " + str(address_list))
        assert recAddress == (hex(int(address, 0))[2:]).upper()
    
        
        recValue = hex_response[index+8:index+16]
        vals.append(recValue)
        
        index = index + 16
    
    #close the socket
    #sock.close()

    #return that list
    return vals
    
    
#write the values in data_list to address at address list. Assumes that the addresses and values are hex strings: '0xFFFFFFFF'
def write_udp(address_list,data_list,sock, host,buf):

    #For each address in the array add it to the request
    header = hex(int("0xE55C0003", 0))[2:]
    
    message_bytes = bytes.fromhex(header)

    for index in range(len(address_list)) :
               
        message_bytes += bytes.fromhex(hex(int(address_list[index], 0))[2:])
        message_bytes += bytes.fromhex((hex(int(data_list[index], 0))[2:]).zfill(8))
       
    #Make the request
    sent = sock.sendto(message_bytes, host)

    #Parse the response, adding it to a response list
    rec_data, rec_host = sock.recvfrom(buf)
    
    hex_response = (str(binascii.hexlify(rec_data))[2:-1]).upper()
       
    recHeader = hex_response[0:4]
    print("Testing Header for: " + str(address_list))
    assert recHeader == "E55C"
    
    for index in range(len(address_list)) :
    
        
      
        #Test that the address we are sent is what we asked for
        recAddress = hex_response[8+index*16:16+index*16]
        print("Testing Address Echo for : " + str(address_list))
        assert recAddress == (hex(int(address_list[index], 0))[2:]).upper() 
    
        #Test that the data we are sent is what we asked for (echo)
        recValue = hex_response[16+index*16:24+index*16]
        print("Testing Value Echo for : " + str(address_list))
        assert recValue == ((hex(int(data_list[index], 0))[2:]).zfill(8)).upper()
        
    recMessage = hex_response[4:8]
    
    
    
 
 # open input file, load json data for reading
def json_parse(json_file):
    fin = open(json_file, "r")
    data = json.load(fin)
    return fin, data