import socket
import sys
import binascii
import time
import math

if len(sys.argv) != 3:
    print("Error - please specify address and data.")
    sys.exit()


DEST_IP_ADDR = '192.168.10.2'
DEST_UDP_PORT = 65535
BUFFER_SIZE = 1024

WRITE_HEAD = hex(int("0xE55C0003", 0))[2:]

ADDR = int(sys.argv[1], 0)
DATA = int(sys.argv[2], 0)

PAYLOAD = hex(ADDR)[2:] + hex(DATA)[2:]

print('Message is: ' + str(WRITE_HEAD) + str(PAYLOAD))

PAYLOAD_BYTES = bytes.fromhex(PAYLOAD)


NUM_PKTS = 10000
NUM_WORDS = int(math.floor((1460 - len(WRITE_HEAD))/8))
#print(NUM_WORDS)
NUM_BYTES = NUM_WORDS*4

host = (DEST_IP_ADDR, DEST_UDP_PORT)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

start_time = time.time()

for packets in range(0, NUM_PKTS):

    MESSAGE = bytes.fromhex(WRITE_HEAD)

    for i in range(0, NUM_WORDS):
    #for i in range(0, 100):
        MESSAGE = MESSAGE + PAYLOAD_BYTES

    sent = sock.sendto(MESSAGE, host)

    rec_data, rec_host = sock.recvfrom(BUFFER_SIZE)


end_time = time.time()
run_time = end_time - start_time

print('Transfer took approximately {} seconds to transfer {} bytes over {} packets'.format(run_time, NUM_BYTES*NUM_PKTS, NUM_PKTS))
print('Throughput was {} kB/s'.format(NUM_BYTES*NUM_PKTS/run_time/1000))

sock.close()
