import socket
import sys
import binascii



DEST_IP_ADDR = '192.168.10.2'
DEST_UDP_PORT = 65535
BUFFER_SIZE = 1024

READ_HEAD = hex(int("0xE55C2001", 0))[2:]



print('Message is: ' + str(READ_HEAD))

MESSAGE_BYTES = bytes.fromhex(READ_HEAD)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = (DEST_IP_ADDR, DEST_UDP_PORT)
sent = sock.sendto(MESSAGE_BYTES, host)

rec_data, rec_host = sock.recvfrom(BUFFER_SIZE)

hex_response = (str(binascii.hexlify(rec_data))[2:-1]).upper()

recHeader = hex_response[0:4]
assert recHeader == "E55C"
print("0x" + recHeader)


recMessage = hex_response[4:8]
assert recMessage == "3002"
print("0x" + recMessage)


sock.close()
